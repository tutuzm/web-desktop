window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  Helloworld: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e1b90/rohdEk4SdmmEZANaD", "Helloworld");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Helloworld = function(_super) {
      __extends(Helloworld, _super);
      function Helloworld() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.Bg1 = null;
        _this.Bg2 = null;
        _this.socer = null;
        _this.birdhome = null;
        _this.gameover = null;
        _this.bird0_0 = null;
        _this.bird0_1 = null;
        _this.bird0_2 = null;
        _this.pipehome1 = null;
        _this.pipehome2 = null;
        _this.pipehome3 = null;
        _this.time = 0;
        _this.speeb = 0;
        _this.stop = false;
        _this.socerNumber = 0;
        return _this;
      }
      Helloworld.prototype.start = function() {
        this.pipehome1.y = -100 + Math.round(300 * Math.random());
        this.pipehome2.y = -100 + Math.round(300 * Math.random());
        this.pipehome3.y = -100 + Math.round(300 * Math.random());
        this.gameover.node.active = false;
        this.socer.string = this.socerNumber.toString();
      };
      Helloworld.prototype.update = function(bt) {
        if (this.stop) {
          console.log("gomeover");
          return;
        }
        this.speeb -= cc.winSize.height / 4e3;
        this.birdhome.rotation = 5 * -this.speeb;
        this.birdhome.y += this.speeb;
        this.time += bt;
        if (this.time > .2) {
          if (this.bird0_0.node.active) {
            this.bird0_1.node.active = true;
            this.bird0_0.node.active = false;
          } else if (this.bird0_1.node.active) {
            this.bird0_2.node.active = true;
            this.bird0_1.node.active = false;
          } else if (this.bird0_2.node.active) {
            this.bird0_0.node.active = true;
            this.bird0_2.node.active = false;
          }
          this.time = 0;
        }
        this.Bg1.x -= 2;
        this.Bg1.x <= -640 && (this.Bg1.x = 640);
        this.Bg2.x -= 2;
        this.Bg2.x <= -640 && (this.Bg2.x = 640);
        this.updatePipeHome(this.pipehome1);
        this.updatePipeHome(this.pipehome2);
        this.updatePipeHome(this.pipehome3);
        this.checkCollision(this.birdhome, this.pipehome1);
        this.checkCollision(this.birdhome, this.pipehome2);
        this.checkCollision(this.birdhome, this.pipehome3);
      };
      Helloworld.prototype.updatePipeHome = function(pipeHome) {
        pipeHome.x -= 2;
        if (pipeHome.x < -345) {
          pipeHome.x = 320;
          pipeHome.y = -100 + Math.round(300 * Math.random());
        }
      };
      Helloworld.prototype.onButtonClick = function() {
        this.speeb = cc.winSize.height / 200;
      };
      Helloworld.prototype.checkCollision = function(birdhome, pipeHome) {
        if (birdhome.x + 17 >= pipeHome.x - 23 && birdhome.x - 17 <= pipeHome.x) if (birdhome.y <= pipeHome.y + 62 - 17 && birdhome.y >= pipeHome.y - 62 + 17) console.log("\u5c0f\u9e1f\u7a7f\u8fc7\u4e2d\u5fc3\u4e86"); else {
          this.stop = true;
          this.gameover.node.active = true;
          console.log("\u5c0f\u9e1f\u6b7b\u4e86");
        } else if (birdhome.x == pipeHome.x + 24) {
          this.socerNumber++;
          this.socer.string = this.socerNumber.toString();
        }
      };
      __decorate([ property(cc.Node) ], Helloworld.prototype, "Bg1", void 0);
      __decorate([ property(cc.Node) ], Helloworld.prototype, "Bg2", void 0);
      __decorate([ property(cc.Label) ], Helloworld.prototype, "socer", void 0);
      __decorate([ property(cc.Node) ], Helloworld.prototype, "birdhome", void 0);
      __decorate([ property(cc.Sprite) ], Helloworld.prototype, "gameover", void 0);
      __decorate([ property(cc.Sprite) ], Helloworld.prototype, "bird0_0", void 0);
      __decorate([ property(cc.Sprite) ], Helloworld.prototype, "bird0_1", void 0);
      __decorate([ property(cc.Sprite) ], Helloworld.prototype, "bird0_2", void 0);
      __decorate([ property(cc.Node) ], Helloworld.prototype, "pipehome1", void 0);
      __decorate([ property(cc.Node) ], Helloworld.prototype, "pipehome2", void 0);
      __decorate([ property(cc.Node) ], Helloworld.prototype, "pipehome3", void 0);
      Helloworld = __decorate([ ccclass ], Helloworld);
      return Helloworld;
    }(cc.Component);
    exports.default = Helloworld;
    cc._RF.pop();
  }, {} ]
}, {}, [ "Helloworld" ]);
//# sourceMappingURL=index.js.map
