window._CCSettings = {
    platform: "web-mobile",
    groupList: [
        "default"
    ],
    collisionMatrix: [
        [
            true
        ]
    ],
    hasResourcesBundle: false,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/test.fire",
    orientation: "portrait",
    debug: true,
    jsList: [],
    bundleVers: {
        internal: "c9161",
        main: "36d1b"
    }
};
